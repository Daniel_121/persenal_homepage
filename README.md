# README #

My personal website.

### What is this repository for? ###

* The playground for learning full-stack JavaScript web developing
* 0.0.0.18


### How do I get set up? ###

* Environment requirement:
* node 6.0.0+
* Mongo 3.4+
* npm install lwip
      https://github.com/nodejs/node-gyp#installation
### Contribution guidelines ###

* Update repo
* Create new branch with same name as issue number
* Pull request
* Code review