$(document).ready(function () {
    if(!$(".nav").find(".active").removeClass("active")){
        $(".home").addClass("active")
    }
    // change header's "active" class status
    $(".nav a").on("click", function(){
        $(".nav").find(".active").removeClass("active");
        $(this).parent().addClass("active");
    });

    if($(location).attr('href').match(/.*\/book_.*/)){
        changeNavActive(".book_store");
    }
    // else if($(location).attr('href').match(/.*\/book_form/)){
    //     changeNavActive(".book_store");
    // }
    else if($(location).attr('href').match(/.*\/house_scheduling/)){
        changeNavActive(".house_scheduling");
    }
    else if($(location).attr('href').match(/.*\/blog_zone/)){
        changeNavActive(".blog_zone");
    }
    else if($(location).attr('href').match(/.*\/about/)){
        changeNavActive(".about");
    }
    else{
        changeNavActive(".home");
    }
});

function changeNavActive(navIndex){
    $(".nav").find(".active").removeClass("active");
    $(navIndex).addClass("active");
}