//-----variables--------

var readyToUpload = true;

//-----page effect------

$('.datePicker').datepicker({
    
    format: "dd/mm/yyyy",
    startDate: "01-01-1970",
    todayBtn: "linked",
    autoclose: true,
    todayHighlight: true
});

$("#bsalert").on("click", toggleAlert)

function toggleAlert(){
    $("#img-alert").toggleClass('in out')
    return false
}

function previewImg(){
    
    console.log("hello please behave normal")
    var preview = document.querySelector('img');
    var file    = document.querySelector('input[type=file]').files[0];
    var reader  = new FileReader();

    reader.addEventListener("load", function () {
        preview.src = reader.result;
    }, false);

    if (file) {
        reader.readAsDataURL(file);
    }
}

//-----page lifecycle controll------

var onLoad = () => {
    console.log("load book form page");
    document.getElementById('submitButton').addEventListener("click", (event)=>{
        event.preventDefault();
    })
}

//-----behave controller-----------

var formHandler = () => {
    if(readyToUpload){
        if($("#img-alert").hasClass("in")){
            $("#img-alert").toggleClass('in out')
        }
        var form = document.getElementById('bookInfo');
        var formData = new FormData(form);
        $.ajax({
            type: 'post',
            url: '/book_form',
            data: formData,
            processData: false,
            contentType: false
        }).done(function(){
            document.getElementById("bookInfo").reset();
            $('img').attr('src', '');
            alert("done")
        })
    }else{
        if($("#img-alert").hasClass("out")){
            $("#img-alert").toggleClass('in out')
        }
    }
}






//----------------------reset orientation by canvas------------------
//
// function changeOritation(img, inputElement) {
        
//         console.log("get orientation info")
//         if(!inputElement){
//             EXIF.getData(img, function() {
//                 var orientation = EXIF.getTag(this, "Orientation");
//                 var originalImage = img,
//                 resetImage = img;
//                 resetOrientation(originalImage.src, orientation, function(resetBase64Image) {
//                         resetImage.src = resetBase64Image;
//                 });
//             });
//         }else{
//             EXIF.getData(img, function() {
//                 var orientation = EXIF.getTag(this, "Orientation");
//                 var originalImage = img,
//                 resetImage = img;
//                 resetOrientation(originalImage.src, orientation, function(resetBase64Image) {
//                         inputElement.files[0] = resetBase64Image;
//                 });
//             });
//         }
        
// }

// function resetOrientation(srcBase64, srcOrientation, callback) {
// 	var img = new Image();	

// 	img.onload = function() {
//                 var     width = img.width,
//                         height = img.height,
//                         canvas = document.createElement('canvas'),
//                         ctx = canvas.getContext("2d");
                        
//                 // set proper canvas dimensions before transform & export
//                 if ([5,6,7,8].indexOf(srcOrientation) > -1) {
//                         canvas.width = height;
//                         canvas.height = width;
//                 } else {
//                         canvas.width = width;
//                         canvas.height = height;
//                 }
                
//                 // transform context before drawing image
//                 switch (srcOrientation) {
//                         case 2: ctx.transform(-1, 0, 0, 1, width, 0); break;
//                         case 3: ctx.transform(-1, 0, 0, -1, width, height ); break;
//                         case 4: ctx.transform(1, 0, 0, -1, 0, height ); break;
//                         case 5: ctx.transform(0, 1, 1, 0, 0, 0); break;
//                         case 6: ctx.transform(0, 1, -1, 0, height , 0); break;
//                         case 7: ctx.transform(0, -1, -1, 0, height , width); break;
//                         case 8: ctx.transform(0, -1, 1, 0, 0, width); break;
//                         default: ctx.transform(1, 0, 0, 1, 0, 0);
//                 }

//                 // draw image
//                 ctx.drawImage(img, 0, 0);

//                 // export base64
//                 callback(canvas.toDataURL());
//         };

// 	img.src = srcBase64;
// }