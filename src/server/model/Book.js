var Book = (()=>{
    function Book(bookinfo, coverImage){
        this.info = bookinfo;
        this.image = coverImage;
    }
    return Book;
})();

module.exports = Book;