var mongooes = require('mongooes')

module.exports = mongooes.model('User', {
    user: string,
    email: string,
    password: string
})