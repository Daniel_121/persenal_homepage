var bookStoreRouter = require('./book_store');

var express = require('express'),
    router = express.Router();

router.get('/', function(req, res){
    res.render('home');
});

router.get('/house_scheduling', function(req, res){
    res.render('house_scheduling');
})

router.get('/blog_zone', function(req, res){
    res.render('blog_zone');
})

router.get('/about', function(req, res){
    res.render('about');
});


module.exports = { 
    indexRouter: router,
    bookStoreRouter : bookStoreRouter
};