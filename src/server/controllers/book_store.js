var express = require('express'),
    router = express.Router();

var Gridfs = require('gridfs-stream')
var mongoose = require('mongoose')
var assert = require('assert')

var fs = require('fs');
var exif = require('exif-parser');
var lwip = require('lwip');

var Book = require('../model/Book');

Gridfs.mongo = mongoose.mongo;

router.get('/book_store', (req, res)=>{
    sendBooks(req, res);
});

router.get('/book_form', (req, res)=>{
    res.render('book_form');
})

router.post('/book_form', (req, res) => {
    mongoose.connection.db = req.app.locals.db;
    var gfs = new Gridfs(mongoose.connection.db);

    var info = req.body;
    var file = req.files.photo;
    var data = file.data;
    var ext = "jpg";
    var exifData = false;

    // ext is the extension of the image
    // which means it only support the jpg format image at this moment
    if(ext == "jpg"){
        exifData = exif.create(data).parse();
    }
    lwip.open(data, ext, function(err, image){
        if(err) throw err;
        if(exifData){
            switch( exifData.tags.Orientation ) {
                case 2:
                image = image.batch().flip('x'); // top-right - flip horizontal
                break;
                case 3:
                image = image.batch().rotate(180); // bottom-right - rotate 180
                break;
                case 4:
                image = image.batch().flip('y'); // bottom-left - flip vertically
                break;
                case 5:
                image = image.batch().rotate(90).flip('x'); // left-top - rotate 90 and flip horizontal
                break;
                case 6:
                image = image.batch().rotate(90); // right-top - rotate 90
                break;
                case 7:
                image = image.batch().rotate(270).flip('x'); // right-bottom - rotate 270 and flip horizontal
                break;
                case 8:
                image = image.batch().rotate(270); // left-bottom - rotate 270
                break;
            }
        }else{
            image = image.batch();
        }

        //console.log(image)
        image.toBuffer("jpg", (err, buffer)=>{
            var writeStream = gfs.createWriteStream({
                filename: info.bookTitle,
                metadata: info,
                mode: 'w',
                content_type:file.mimetype
            });

            writeStream.on('close', function() {
                console.log("this is writeStream on close call back");
                res.sendStatus(200);
                //sendBooks(req, res);
            });

            writeStream.write(buffer);
            writeStream.end();
        })
    })



})

function sendBooks(req, res){
    
    db = req.app.locals.db;
    var gfs = new Gridfs(db);
    
    var bookinfo=[];
    var fileCollection = db.collection('fs.files');

    fileCollection.find().count().then((count)=>{
        if(count === 0){
            console.log('get book store no book')
            res.render('book_store', {noBook: true});
        }else{
            console.log('get book store with book')
            fileCollection.find().toArray().then((books)=>{
                //console.log(books);
                books.forEach(function(item) {
                    var readStream = gfs.createReadStream({_id: item._id})
                    var bufs = [];
            
                    readStream.on('error', function(err){
                        console.log('An error occurred!', err);
                        throw err;
                    })

                    readStream.on('data', function(chunk) {
                        bufs.push(chunk);
                    }).on('end', function() { // done
                        var fbuf = Buffer.concat(bufs);
                        //console.log('fbuf: '+fbuf)
                        var base64 = (fbuf.toString('base64'));
                        //console.log('base64: '+base64)
                        var book = new Book(item.metadata, base64);
                        bookinfo.push(book);
                        if(bookinfo.length === books.length){
                            console.log("redirect to book store!")
                            res.render('book_store', {books: bookinfo})
                        }
                    });          
                }, this);
            })
        }
    })
}

module.exports = router;