var express = require('express');
var app = express();
var routers = require('./controllers');

var busBoyBodyParser = require('busboy-body-parser');

var path = require('path');
var favicon = require('serve-favicon');

var MongoClient = require('mongodb').MongoClient
  , assert = require('assert');

var MongoConf = require('./configure/mongo/mongodb.configure');

app.set('viwes', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(busBoyBodyParser());

app.use(favicon(path.join(__dirname,'public','images', 'favicon','favicon.ico')));

app.use(routers.indexRouter);
app.use(routers.bookStoreRouter);

app.use(express.static(__dirname + '/public'));

MongoClient.connect(MongoConf.url, function(err, db) {
  assert.equal(null, err);
  console.log("Connected successfully to server");
  app.locals.db = db;
  app.locals.assert = assert;
  app.listen(1337, function(){
        console.log('ready on port 1337');
    })
  //db.close();
});

